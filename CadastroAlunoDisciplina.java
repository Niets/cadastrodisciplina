public class CadastroAlunoDisciplina{

	public static void main(String[] args)throws IOException{
	
		InputStream entradaSistema=System.in;
		InputStreamReader leitor= new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader(leitor);
		String entradaTeclado;
		char menuOpcao;
		
			ControleDisciplina umControleDisciplina= new ControleDisciplina();
			String nomeDisciplina;
			
			ControleAluno umControleAluno= new ControleAluno();
			String nomeAluno;
			
			
			
			do{
				System.out.println("============MENU==========");
				System.out.println("INSIRA A OPÇÃO DESEJADA");
				System.out.println("1 = ADICIONAR ALUNO");
				System.out.println("2 = ADICIONAR DISCIPLINA");
				System.out.println("3 = ADICIONAR A ALUNO A DISCIPLINA");
				System.out.println("4 = BUSCAR ALUNO ");
				System.out.println("5 = BUSCAR DISCIPLINA");
				System.out.println("6 = REMOVER ALUNO");
				System.out.println("7 = REMOVER DISCIPLINA");
				System.out.println("8 = LISTAR ALUNOS");
				System.out.println("9 = LISTAR DISCIPLINAS");
				System.out.println("============MENU==========");
				
				switch(menuOpcao){
				
					case'1':
						System.out.println("Digite o nome do aluno:");
						entradaTeclado = leitorEntrada.readLine();
						String umNome = entradaTeclado;
						System.out.println("Digite a matrícula do aluno:");
						entradaTeclado = leitorEntrada.readLine();
						String umaMatricula = entradaTeclado;
						
						Aluno umAluno = new Aluno(umNome, umaMatricula);
						
						String mensagem = umControleAluno.adicionarAluno(umAluno);
						break;
						
					case'2':
						System.out.println("Digite o nome da disciplina:");
						entradaTeclado = leitorEntrada.readLine();
						String umNomeDisciplina = entradaTeclado;
						System.out.println("Digite o código da disciplina:");
						entradaTeclado = leitorEntrada.readLine();
						String umCodigo = entradaTeclado;
						
						Disciplina umaDisciplina = new Disciplina(umNomeDisciplina, umCodigo);
						
						String mensagem = umControleDisciplina.adicionarDisciplina(umaDisciplina);
						break;
						
					case'3':
						System.out.println("Digite o nome da disciplina:");
						entradaTeclado = leitorEntrada.readLine();
						umControleAluno.pesquisarAluno(entradaTeclado);
						break;
						
					case'4':
						System.out.println("Digite o nome do aluno:");
						entradaTeclado = leitorEntrada.readLine();
						umControleAluno.pesquisarAluno(entradaTeclado);
						break;
					
					case'5':
						System.out.println("Digite o nome do Aluno:");
						entradaTeclado = leitorEntrada.readLine();
						umAluno = umControleAluno.pesquisarNome(entradaTeclado);
						System.out.println("Digite o nome da Disciplina:");
						entradaTeclado = leitorEntrada.readLine();
						umaDisciplina = umControleDisciplina.pesquisarDisciplina(entradaTeclado); 
						break;
						
					case'6':
						umControleAluno.exibirAlunos();
						System.out.println("Digite o nome do aluno à ser removido:");
						entradaTeclado = leitorEntrada.readLine();
						nomeAluno = entradaTeclado;
						
						umAluno = umControleAluno.pesquisarNome(nomeAluno);
						umControleAluno.remove(umAluno);
						break;
	





}
