import java.util.arraylist;

public class Disciplina {
		
		private ArrayList<Aluno> listaAlunos;
		private String nomeDisciplina;
		private String codigo;
		
		public Disciplina(){
			listaAlunos = new ArrayList<Aluno>();
		}
		
		public String adicionarAlunoDisciplina(Aluno umAluno){
			String mensagem = "Aluno Adicionado à Disciplina com Sucesso";
			listaDisciplinas.add(umAluno);
			return mensagem;
		}
		
		public String removerAlunoDisciplina(Aluno umAluno){
			String mensagem = "Aluno Removido da Disciplina com Sucesso";
			listaDisciplinas.remove(umAluno);
			return mensagem;
			}
			
		public void exibirAlunosDisciplinas(){
			for(Aluno umAluno: listaAlunos){
				System.out.println(umAluno.getNome());
			}
		} 	
		
		public String getCodigo(){
			return this.codigo;
		}

		public String getNomeDisciplina(){
			return this.nomeDisciplina;
		}
		
		public void setCodigo(String umCodigo){
			codigo = umCodigo;
		}
		
		public void setNomeDisciplina(String umNomeDisciplina){
			nomeDisciplina = umNomeDisciplina;
		}
		
		public Alunos getAlunos(){
			return listaAlunos;
		}
	
			
		
}
