public class Aluno{
//atributos
	private String nome;
	private String matricula;
	
// metodos

	public Aluno(){
		nome = "...";
		matricula = "...";
		}
	
	public Aluno(String nome){
		this.nome = nome;	
		}
		
	public Aluno(String nome, String matricula){
		this.nome = nome;
		this.matricula = matricula;
		}
	
	public String getNome(){
	 	return this.nome;
	 	}
	 	
	public void setNome(String umNome){
		nome= umNome;
		}
	
	public String getMatricula(){
		return this.matricula;
		}
		
	public void setMatricula(String umaMatricula){
		matricula = umaMatricula;
		}
		
}		 	
