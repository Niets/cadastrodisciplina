import java.util.arraylist;
public class ControleDisciplina {
		private ArrayList<Disciplina> listaDisciplinas;
		
		public ControleDisciplina(){
			listaDisciplinas = new ArrayList<Disciplinas>();		
		}
		
		public String adicionarDisciplina(Disciplina umaDisciplina){
			String mensagem = "Disciplina Adicionada com Sucesso";
			listaDisciplinas.add(umaDisciplina);
			return mensagem;
		} 
		
		public String removerDisciplina(Disciplina umaDisciplina){
			String mensagem = "Disciplina Removida com Sucesso";
			listaDisciplinas.remove(umaDisciplina);
			return mensagem;

		public Disciplina pesquisarDisciplina(String umNomeDisciplina){
			for(Disciplina umaDisciplina: listaDisciplinas){
				if(umaDisciplina.getNome().equalsIgnoreCase(umNomeDisciplina)){
					return umaDisciplina;
				}
				
			return null;
			}
		}
			
		public void exibirDisciplinas(){
			for(Disciplina umaDisciplina: listaDisciplinas){
				System.out.println(umaDisciplina.getNomeDisciplina());
			}
		} 
			
		
		
			
}
